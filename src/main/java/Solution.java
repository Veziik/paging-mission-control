package main.java;
import java.io.File;  // Import the File class
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.text.ParseException; // Import the Scanner class to read text files
import java.text.SimpleDateFormat; // import the ArrayList class
import java.util.ArrayList; // import the HashMap class
import java.util.Date; // import the Date class
import java.util.HashMap; // import the SimpleDateFormat class
import java.util.Scanner; // import the ParseException class


public class Solution {

  private static final int EVENT_ALERT_OCURRENCES_THRESHOLD = 3;
  private static final int EVENT_ALERT_WINDOW = 5; // in minutes

  // the below data structure will hold the satellite event alerts
  // the first key is the satellite id
  // the second key is the component slug
  // the value is a list of dates when the event occurred
  private final HashMap<Integer,HashMap<String,ArrayList<Date>>> mapOfEventsBySatelliteAndComponent;  
  // the below string will hold the satellite alert report in json format
  private String satelliteAlertReportString = "";
  private final String logFileLocation;

  public Solution(String newLogFileLocation) {
    satelliteAlertReportString = "";
    mapOfEventsBySatelliteAndComponent = new HashMap<>();
    logFileLocation = newLogFileLocation;
  }


  private ArrayList<Date> refreshEventDateList(Date incomingDate, ArrayList<Date> eventDates) {
    /**
     * This method will refresh the list of event dates for a component
     */
    Date windowStartDate = new Date(incomingDate.getTime() - EVENT_ALERT_WINDOW * 60 * 1000);
    ArrayList<Date> datesToRemove = new ArrayList<>();

    for (int i = 0; i < eventDates.size(); i++) {
      if (eventDates.get(i).before(windowStartDate)) {
        datesToRemove.add(eventDates.get(i));
      }
    }

    eventDates.removeAll(datesToRemove);  
    eventDates.add(incomingDate);
    
    return eventDates;
  }

  private void initializeEventAlertsForSatellite(int satteliteId) {
    /**
     * This method will initialize the event alerts for a newly encountered satellite
     */
    HashMap<String,ArrayList<Date>> componentEventAlertsForSatellite = new HashMap<>();
    componentEventAlertsForSatellite.put("TSTAT", new ArrayList<>());
    componentEventAlertsForSatellite.put("BATT", new ArrayList<>());
    mapOfEventsBySatelliteAndComponent.put(satteliteId, componentEventAlertsForSatellite);
  }

  private void generateEventAlertJson(int satteliteId, String componentSlug) {
    String severity = componentSlug.equals("TSTAT") ? "RED HIGH" : "RED LOW";
    Date oldestDate = mapOfEventsBySatelliteAndComponent.get(satteliteId).get(componentSlug).get(0);
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    String oldestDateString = formatter.format(oldestDate); 
    String alertJsonString = "\t{\n";
    // Could I have used JSONOBject here? Yes.
    // Could I have concatenated all the strings in one line? Yes.
    // I'd rather it keep the lines under 140 characters where possible for readability
    alertJsonString += "\t\t\"satelliteId\": " + satteliteId + ",\n";
    alertJsonString += "\t\t\"severity\": \"" + severity + "\",\n";
    alertJsonString += "\t\t\"component\": \"" + componentSlug + "\",\n";
    alertJsonString += "\t\t\"timestamp\": \"" + oldestDateString + "\"";
    alertJsonString += "\n\t}";
    if(!satelliteAlertReportString.contains(alertJsonString)){
      if (satelliteAlertReportString.isEmpty()){
        satelliteAlertReportString = alertJsonString;
      } else {
        satelliteAlertReportString += ",\n" + alertJsonString;
      }
    }
  }

  public void generateAndPrintAlertReport(){
    try {
      File logFile = new File(logFileLocation);
        // Read the log file line by line
        try (Scanner logFileReader = new Scanner(logFile)) {
            while (logFileReader.hasNextLine()) {
                String log = logFileReader.nextLine();
                String[] logData = log.split("\\|");
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
                
                // Parse the log data in the line
                Date eventDate = formatter.parse(logData[0]);
                int satteliteId = Integer.parseInt(logData[1]);
                float redHighLimit = Float.parseFloat(logData[2]);
                float redLowLimit = Float.parseFloat(logData[5]);
                float rawValue = Float.parseFloat(logData[6]);
                String componentSlug = logData[7];
                
                // Check if the satellite is already in the map
                // If not, initialize the event alerts for the satellite
                if (!mapOfEventsBySatelliteAndComponent.containsKey(satteliteId)) {
                    initializeEventAlertsForSatellite(satteliteId);
                }
                
                // Check if the raw value is out of the red limits depending on the component
                String offendingComponentSlug = "";
                if((rawValue > redHighLimit) && componentSlug.equals("TSTAT")) {
                    offendingComponentSlug = "TSTAT";
                } else if((rawValue < redLowLimit) && componentSlug.equals("BATT")) {
                    offendingComponentSlug = "BATT";
                }
                
                // If the component is offending, refresh the event date list and check if the alert threshold is reached
                if(!offendingComponentSlug.equals("")){
                    ArrayList<Date> componentRedEventDates = mapOfEventsBySatelliteAndComponent.get(satteliteId).get(offendingComponentSlug);
                    componentRedEventDates = refreshEventDateList(eventDate, componentRedEventDates);
                    mapOfEventsBySatelliteAndComponent.get(satteliteId).put(offendingComponentSlug, componentRedEventDates);
                    
                    // If we reach the threshold, generate the event alert json string
                    // as long as it's not already in the satellite event alert json string
                    if (componentRedEventDates.size() >= EVENT_ALERT_OCURRENCES_THRESHOLD) {
                        generateEventAlertJson(satteliteId, componentSlug);
                    }
                }
            } }

      // Print the satellite event alerts after processing the log file
      System.err.println(getSatteliteAlertReportString());
    } catch (FileNotFoundException|ParseException e) {
      System.out.println("An error occurred." + e.getMessage());
    }
  }

  public static void main(String[] args) {
    // check if the log file location is provided as an argument
    if (args.length == 0) {
      System.out.println("Please provide the log file location as an argument");
      return;
    }
    String logFileLocation = args[0];
    Solution solution = new Solution(logFileLocation);
    solution.generateAndPrintAlertReport();
  }

    public static int getEventAlertOccurencesThreshold() {
        return EVENT_ALERT_OCURRENCES_THRESHOLD;
    }

    public static int getEventAlertWindow() {
        return EVENT_ALERT_WINDOW;
    }

    public String getSatteliteAlertReportString() {
        return "[\n" + satelliteAlertReportString + "\n]";
    }
}

