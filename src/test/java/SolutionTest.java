import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

import main.java.Solution;

public class SolutionTest{

    private void helperValidateOutputFromLogFile(String resourceLocationString) throws IOException {
        /*
         *  Helper Function for all of the tests that will be run on the Solution
         * 
         *  I originally wanted to capture the STDOUT and compare it to the expected output
         * but doing so in Java is way less intuitive than in Python. I decided to instead
         * save the final state of the output into the class itself and use that saved value
         * to compare to the expected output. 
         * 
         * I also decided to throw the IOException in this function so that the test functions
         * dont just pass if the file is not found.
         */
        String logFileLocation = "src/test/resources/log-files/" + resourceLocationString + ".txt";
        String expectedOutputFileLocationString = "src/test/resources/expected-outputs/" + resourceLocationString + ".json";

        // Provide the log file location as an argument
        // Call the main method
        Solution solution = new Solution(logFileLocation);
        solution.generateAndPrintAlertReport();
        String generatedOutput = solution.getSatteliteAlertReportString().replaceAll("\\s+", "");
        String expectedOutput = String.join(
            "", 
            Files.readAllLines
            (
                Paths.get(expectedOutputFileLocationString), 
                StandardCharsets.UTF_8
                )
                ).replaceAll("\\s+", "");
    
        // remove the whitespace from the generated output and the expected output
        // so that the difference between windows and unix line endings is ignored
        String generatedoutputNoWhitespace = generatedOutput.replaceAll("\\s+", "");
        String expectedOutputNoWhitespace = expectedOutput.replaceAll("\\s+", "");

        // Assert the expected output and the actual output are the same
        assertEquals(expectedOutputNoWhitespace, generatedoutputNoWhitespace);

    }

    @Test
    public void testGivenSample() throws IOException {
        /*
         * Test the main method with the given input provided in the README
         */
        helperValidateOutputFromLogFile("given_sample");
    }

    @Test
    public void testOneThermostatEvent() throws IOException{
        /*
         * Test the main method with a log file containing a single thermostat event
         */
        helperValidateOutputFromLogFile("one_thermostat_event");
    }

    @Test
    public void testOneBatteryEvent() throws IOException{
        /*
         * Test the main method with a log file containing a single battery event
         */
        helperValidateOutputFromLogFile("one_battery_event");
    }

    @Test
    public void testDoubleBatteryEvent() throws IOException{
        /*
         * Test the main method with a log file containing two battery events.
         * The goal here is to ensure that the oldest event is being reported.
         */
        helperValidateOutputFromLogFile("double_battery_event");
    }

    @Test
    public void test1000Lines() throws IOException{
        /*
         * Test the main method with a log file containing 1000 events.
         * The goal here is to ensure that the oldest event is being reported.
         */
        helperValidateOutputFromLogFile("1000_lines");
    }
}