import random
import datetime

number_of_satellites = 12
components = ["TSTAT", "BATT"]
red_high_limit = 101
yellow_high_limit = 98
yellow_low_limit = 25
red_low_limit = 20
number_of_logs = 1000
log_file = "test_logs_1000_lines.txt"

timestamp = datetime.datetime(2018, 1, 1, 23, 1, 5, 1)

with open(log_file, "w") as file:
    satellite_ids = [str(i) for i in range(1000, 1000 + number_of_satellites)]
    for _ in range(number_of_logs):
        satellite_id = random.choice(satellite_ids)
        component = random.choice(components)
        if component == "TSTAT":
            raw_value = random.uniform(80, 105)
        else:  # BATT
            raw_value = random.uniform(5, 20)
        log_message = f"{timestamp.strftime('%Y%m%d %H:%M:%S.%f')[:-3]}|{satellite_id}|{red_high_limit}|{yellow_high_limit}|{yellow_low_limit}|{red_low_limit}|{raw_value:.1f}|{component}\n"
        file.write(log_message)
        timestamp += datetime.timedelta(seconds=random.randint(1, 10))