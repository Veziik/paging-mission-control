#! usr/bin/env python3
import threading
import datetime
import json


# I will be providing a solution to this problem as if I were expecting the operation to be scaled up to a large number
# of satellites and read a large number of logs.

# Possible expansion of this code could include file change monitoring so that the main thread could be run as a daemon
# but that requires nonstandard libraries and is definitely out of scope for this code exercise
# [threading it kinda already is]

# I will also be operating under the assumption that if alert-worthy events continue to occur consecutively for
# longer than the 5-minute window, we should continue to write alerts as long as the oldest timestamp in the set of
# events is newer than the timestamp of the last alert written.

def _oldest_timestamp(set_of_event_tuples):
    oldest_timestamp = datetime.datetime.now()
    for event in set_of_event_tuples:
        timestamp = event[0]
        if timestamp < oldest_timestamp:
            oldest_timestamp = timestamp
    return oldest_timestamp


class SatelliteTracker(threading.Thread):
    # The purpose of this class is serve as an abstraction for a connection to a satellite via the input logs
    # that way we can have each tracker run in its own thread and check for red limit events independently
    # of the other trackers. This is partially to keep the solution scalable, but also to show off a bit.
    OUTPUT_JSON_WRITE_LOCK = threading.Lock()
    ALERT_THRESHOLD_MINUTES = datetime.timedelta(minutes=5)
    ALERT_THRESHOLD_OCCURRENCES_NUMBER = 3
    log_messages = list()
    trackers = list()
    _alerts_json = list()

    def __init__(self, satellite_id):
        super().__init__()
        self._debug_logs = []
        self.satellite_id = satellite_id

        # These sets hold tuples of the form (timestamp, value) for each event that occurs
        # we don't actually care about the order of these items, so long as all the events occur within the
        # 5-minute window
        self.red_low_battery_events = set()
        self.red_high_thermostat_events = set()

    @staticmethod
    def initialize(log_messages):
        SatelliteTracker.log_messages = log_messages
        SatelliteTracker._alerts_json = list()
        # Get a list of all the different Satellite IDs in the logs to create a tracker for each
        satellite_ids = set()
        for log in log_messages:
            satellite_ids.add(int(log.split('|')[1]))

        # Create a tracker for each satellite ID
        trackers = set()
        for satellite_id in satellite_ids:
            trackers.add(SatelliteTracker(satellite_id))

        SatelliteTracker.trackers = trackers

        return trackers

    @staticmethod
    def start_tracking():
        for tracker in SatelliteTracker.trackers:
            tracker.start()

    @staticmethod
    def await_results():
        for tracker in SatelliteTracker.trackers:
            tracker.join()

    def _write_to_alert_json(self, current_log_component_name: str):
        """
        writes an alert to the alerts json in a thread-safe manner, but only if the alert is novel given the current
        oldest timestamp in the set of events
        :param current_log_component_name: the component name that triggered the alert
        :return:
        """
        with self.OUTPUT_JSON_WRITE_LOCK:
            if current_log_component_name == 'BATT':
                timestamp = _oldest_timestamp(self.red_low_battery_events)
                severity = "RED LOW"
            else:
                timestamp = _oldest_timestamp(self.red_high_thermostat_events)
                severity = "RED HIGH"

            alert_dict = {
                "satelliteId": self.satellite_id,
                "severity": severity,
                "component": current_log_component_name,
                "timestamp": timestamp.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z',
            }

            if alert_dict not in self._alerts_json:
                self._alerts_json.append(alert_dict)

    def dump_debug_logs(self):
        """
        dumps the debug logs to stdout, good for avoiding race conditions in the stdout while debugging
        :return:
        """
        for log in self._debug_logs:
            print(log)

    @staticmethod
    def dump_alert_json():
        """"
        Dumps the alerts json in a human-readable format to stdout
        also returns the formatted json string
        :return: the formatted json string
        """
        sorted_list = sorted(SatelliteTracker._alerts_json, key=lambda d: d['timestamp'])
        sorted_list.reverse()
        formatted_list = json.dumps(sorted_list, indent=4)
        print(formatted_list)
        return formatted_list

    def _write_to_debug_log(self, log):
        """
        Write a log message to the debug logs for later dumping
        :param log: a log message
        """
        self._debug_logs.append(log)

    def __str__(self):
        return f"SatelliteTracker({self.satellite_id})"

    def _check_for_red_limit_events(self):
        """
        This method checks for red limit events in the log messages and writes alerts to the alerts json if the
        threshold is met within the given time limit
        :return:
        """
        self._write_to_debug_log(f"Thread {self} - checking for red limit events")
        self._write_to_debug_log(f"Thread {self} - LOG_MESSAGES: {SatelliteTracker.log_messages}")
        for line in SatelliteTracker.log_messages:
            self._write_to_debug_log(f"Thread {self} - line: {line}")
            line_split = line.split('|')
            current_log_timestamp = datetime.datetime.strptime(line_split[0], '%Y%m%d %H:%M:%S.%f')
            current_log_satellite_id = int(line_split[1])
            current_log_red_high_limit = float(line_split[2])
            current_log_red_low_limit = float(line_split[5])
            current_log_raw_value = float(line_split[6])
            current_log_component_name = line_split[7]

            if current_log_satellite_id == self.satellite_id:
                if current_log_component_name == 'BATT':
                    # each time we get a new battery reading, we need to do a couple of things
                    # 1. Check the timestamp of the new reading and remove any events
                    #   older than 5 minutes from self.red_low_battery_events
                    # 2. Check if the new reading is below the red low limit and add it to self.red_low_battery_events
                    # 3. If the length of self.red_low_battery_events is greater than 3, write an alert
                    self._write_to_debug_log(f"Thread {self.satellite_id} - battery level: {current_log_raw_value}, "
                                             f"red low limit: {current_log_red_low_limit}")

                    if current_log_raw_value <= current_log_red_low_limit:
                        self._write_to_debug_log(f"Thread {self.satellite_id} - LOW BATTERY EVENT FOUND: {line}")
                        events_to_remove = set()
                        for event in self.red_low_battery_events:
                            event_timestamp = event[0]
                            if current_log_timestamp - event_timestamp > SatelliteTracker.ALERT_THRESHOLD_MINUTES:
                                events_to_remove.add(event)
                        self.red_low_battery_events = self.red_low_battery_events - events_to_remove
                        self.red_low_battery_events.add((current_log_timestamp, current_log_raw_value))

                    if len(self.red_low_battery_events) >= SatelliteTracker.ALERT_THRESHOLD_OCCURRENCES_NUMBER:
                        self._write_to_debug_log(
                            f"Thread {self.satellite_id} - LOW BATTERY EVENT THRESHOLD MET: {line}")
                        self._write_to_alert_json(current_log_component_name)

                elif current_log_component_name == 'TSTAT':
                    self._write_to_debug_log(f"Thread {self.satellite_id} - thermostat level: {current_log_raw_value}, "
                                             f"red high limit: {current_log_red_high_limit}")
                    # the logic for this is the same as the battery logic, but instead of red low limits, we are
                    # checking for red high limits

                    if current_log_raw_value >= current_log_red_high_limit:
                        self._write_to_debug_log(f"Thread {self.satellite_id} - HIGH TSTAT EVENT FOUND: {line}")
                        events_to_remove = set()
                        for event in self.red_high_thermostat_events:
                            event_timestamp = event[0]
                            if current_log_timestamp - event_timestamp > SatelliteTracker.ALERT_THRESHOLD_MINUTES:
                                events_to_remove.add(event)
                        self.red_high_thermostat_events = self.red_high_thermostat_events - events_to_remove
                        self.red_high_thermostat_events.add((current_log_timestamp, current_log_raw_value))

                    if len(self.red_high_thermostat_events) >= SatelliteTracker.ALERT_THRESHOLD_OCCURRENCES_NUMBER:
                        self._write_to_debug_log(f"Thread {self.satellite_id} - HIGH TSTAT EVENT THRESHOLD MET: {line}")
                        self._write_to_alert_json(current_log_component_name)

    def run(self):
        self._check_for_red_limit_events()


def parse_logs(log_messages, debug=False):
    # Create a tracker for each satellite ID
    satellite_trackers = SatelliteTracker.initialize(log_messages)
    print(f"satellite_trackers: {satellite_trackers}") if debug else None

    # start the satellite_trackers
    SatelliteTracker.start_tracking()
    print("Trackers started") if debug else None
    print(f"satellite_trackers: {satellite_trackers}") if debug else None

    # wait for the satellite_trackers to finish since the log file is finite in our case
    SatelliteTracker.await_results()

    # dump the debug logs
    if debug:
        print("Dumping debug logs")
        for tracker in satellite_trackers:
            tracker.dump_debug_logs()

    SatelliteTracker.dump_alert_json()


if __name__ == '__main__':
    log_path = 'test_logs_1000_lines.txt'
    log_message_lines = []
    with open(log_path, 'r') as f:
        for line in f.readlines():
            log_message_lines.append(line.replace('\n', ''))
    parse_logs(log_message_lines)
