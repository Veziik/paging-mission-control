import unittest
import io
import contextlib
import json
from solution import SatelliteTracker


def get_log_lines_from_file(file_path):
    log_lines = []
    with open(file_path, 'r') as f:
        for line in f.readlines():
            log_lines.append(line.replace('\n', ''))
    return log_lines


def load_json_from_file(file_path):
    with open(file_path, 'r') as f:
        return json.loads(f.read())


MOCK_ONE_THERMOSTAT_ALERT = {
    "logs": [
        '20180101 23:01:05.001|1001|101|98|25|20|102.9|TSTAT',
        '20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT',
        '20180101 23:01:26.011|1001|101|98|25|20|102.9|TSTAT',
        '20180101 23:01:38.001|1001|101|98|25|20|102.9|TSTAT'
    ],
    "output":
        [
            {
                "satelliteId": 1001,
                "severity": "RED HIGH",
                "component": "TSTAT",
                "timestamp": "2018-01-01T23:01:05.001Z"
            }
        ],
}

MOCK_ONE_BATTERY_ALERT = {
    "logs": [
        '20180101 23:01:05.001|1001|101|98|25|20|102.9|TSTAT',
        '20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT',
        '20180101 23:01:26.011|1000|17|15|9|8|7.8|BATT',
        '20180101 23:01:38.001|1000|17|15|9|8|7.8|BATT',
    ],
    "output":
        [
            {
                "satelliteId": 1000,
                "severity": "RED LOW",
                "component": "BATT",
                "timestamp": "2018-01-01T23:01:09.521Z"
            }
        ],
}

MOCK_DOUBLE_BATTERY_ALERT = {
    "logs": [
        '20180101 23:01:05.001|1001|101|98|25|20|102.9|TSTAT',
        '20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT',
        '20180101 23:01:26.011|1000|17|15|9|8|7.8|BATT',
        '20180101 23:01:38.001|1000|17|15|9|8|7.8|BATT',
        '20180101 23:01:35.001|1000|17|15|9|8|7.8|BATT',
    ],
    "output":
        [
            {
                "satelliteId": 1000,
                "severity": "RED LOW",
                "component": "BATT",
                "timestamp": "2018-01-01T23:01:09.521Z"
            },
        ],
}

MOCK_LARGE_DATASET = {
    "logs": get_log_lines_from_file('test_logs_1000_lines.txt'),
    "output": load_json_from_file('expected_output_1000_lines.json')
}

MOCK_PROVIDED_SAMPLE_DATA = {
    "logs": get_log_lines_from_file('test_logs_given_sample.txt'),
    "output":
        [
            {
                "satelliteId": 1000,
                "severity": "RED HIGH",
                "component": "TSTAT",
                "timestamp": "2018-01-01T23:01:38.001Z"
            },
            {
                "satelliteId": 1000,
                "severity": "RED LOW",
                "component": "BATT",
                "timestamp": "2018-01-01T23:01:09.521Z"
            }
        ],
}


class SatelliteTrackerTests(unittest.TestCase):

    def run_satellite_tracker_and_verify_output(self, test_data, debug=False):
        SatelliteTracker.initialize(test_data['logs'])
        SatelliteTracker.start_tracking()
        SatelliteTracker.await_results()
        with contextlib.redirect_stdout(io.StringIO()) as f:
            SatelliteTracker.dump_alert_json()
            generated_output = f.getvalue()
        expected_output = test_data['output']
        if debug:
            print("Expected Output:")
            print(expected_output)
            print("Generated Output:")
            print(generated_output)
        self.assertEqual(expected_output, json.loads(generated_output))

    def test_satellite_tracker_initialization(self):
        satellite_id = '1001'
        tracker = SatelliteTracker(satellite_id)
        self.assertEqual(tracker.satellite_id, '1001')

    def test_check_for_one_thermostat_alert(self):
        """
        Tests that the SatelliteTracker class can correctly identify a thermostat alert
        While also ignoring battery a single battery event
        :return:
        """
        self.run_satellite_tracker_and_verify_output(MOCK_ONE_THERMOSTAT_ALERT)

    def test_check_for_one_battery_alert(self):
        """
        Tests the inverse of the test_check_for_one_thermostat_alert, here we identify a battery alert
        and ignore a single thermostat event
        :return:
        """
        self.run_satellite_tracker_and_verify_output(MOCK_ONE_BATTERY_ALERT)

    def test_check_for_duplicate_alert(self):
        """
        Tests the case where we have two battery alerts for the same satellite in rapid succession.
        Essentially, if the timestamp of the two alerts is within the threshold, we should only output one alert.

        AKA, no alerts can share a timestamp
        :return:
        """
        self.run_satellite_tracker_and_verify_output(MOCK_DOUBLE_BATTERY_ALERT)

    def test_check_for_given_sample_data(self):
        """
        Here we run the sample data provided in the prompt to verify that the solution works as expected
        :return:
        """
        self.run_satellite_tracker_and_verify_output(MOCK_PROVIDED_SAMPLE_DATA)

    def test_check_for_large_dataset(self):
        """
        The main test, we run the large dataset to verify that the solution can handle a large number of logs.
        We also run the sample multiple times to verify consistency of the output and rule out the existence of
        race condition related bugs.
        :return:
        """
        self.run_satellite_tracker_and_verify_output(MOCK_LARGE_DATASET)
        self.run_satellite_tracker_and_verify_output(MOCK_LARGE_DATASET)
        self.run_satellite_tracker_and_verify_output(MOCK_LARGE_DATASET)
        self.run_satellite_tracker_and_verify_output(MOCK_LARGE_DATASET)


if __name__ == '__main__':
    unittest.main()
